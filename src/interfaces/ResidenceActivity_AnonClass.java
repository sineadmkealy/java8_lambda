package interfaces;

/**
 * 
 * @implements interface TextWatcher
 *
 */
public class ResidenceActivity_AnonClass implements TextWatcher {

	long dateTextChanged;
	
	  TextView textView = new TextView(); // Declare and initialize a TextView field

	public ResidenceActivity_AnonClass() {
		// beforeTextChanged() a static method in interface
		// initialize date field this within the activity constructor
		dateTextChanged = TextWatcher.beforeTextChanged(); 
		
		// Recall the signature of TextView.addTextChangedListener:
		//  addTextChangedListener(TextWatcher textwatcher) : We shall satisfy this parameter requirement with an anonymous class.
		// Anon class from (new to final }
		// a synthetic class is created and this class implements the TextWatcher interface. 
		// An object of the class is then created, a reference to which is passed as a parameter to the method TextView.addTextChangedListener. 
		// This object satisfies the typing requirement of the parameter, namely TextWatcher.
		textView.addTextChangedListener(new TextWatcher() {

			@Override
		      public void afterTextChanged() {
		        System.out.println("Text changed");		
			}
		    });
	}

	@Override
	public void onTextChanged() {
		System.out.println("Adapt or die");
	}

	@Override
	public void afterTextChanged() {
		System.out.println("We're watching you");
	}

}
