package interfaces;

/**
 * A lambda expression could be described as an anonymous function - a block of code that is 
 * capable of expressing behaviour, 
 * is not associated with any class and 
 * that does not have a name.
/**
 * 
 * 
 * @implements interface TextWatcher
 *
 */
public class ResidenceActivity implements TextWatcher {

	long dateTextChanged;
	
	  TextView textView = new TextView(); // Declare and initialize a TextView field

	public ResidenceActivity() {
		// beforeTextChanged() a static method in interface
		// initialize date field this within the activity constructor
		dateTextChanged = TextWatcher.beforeTextChanged(); 
		
		// Recall the signature of TextView.addTextChangedListener:
		//  addTextChangedListener(TextWatcher textwatcher) : We shall satisfy this parameter requirement with an anonymous class.
		// Anon class from (new to final }
		// a synthetic class is created and this class implements the TextWatcher interface. 
		// An object of the class is then created, a reference to which is passed as a parameter to the method TextView.addTextChangedListener. 
		// This object satisfies the typing requirement of the parameter, namely TextWatcher.
		//textView.addTextChangedListener(new TextWatcher() {

			//@Override
		    //  public void afterTextChanged() {
		    //    System.out.println("Text changed");		
			//}
		  //  });
		
		//Lambda replacing anonymous class
	    textView.addTextChangedListener(() -> System.out.println("Text changed"));
	  }
	

	@Override
	public void onTextChanged() {
		System.out.println("Adapt or die");
	}

	@Override
	public void afterTextChanged() {
		System.out.println("We're watching you");
	}
	
	//adding a second abstract method to TextWatcher. 
	//In doing so, TextWatcher ceases to be a functional interface.
	void onBeforeTextChanged(); // erro caused by an attempt to convert a non-functional interface to a lambda expression

}
