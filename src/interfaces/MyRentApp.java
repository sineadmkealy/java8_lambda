package interfaces;

import java.util.Date;

/**
 *
 * @author Sinead
 *
 */
public class MyRentApp {

	  public static void main(String[] args) {
		  
	    ResidenceActivity activity = new ResidenceActivity();
	    
	    // these are methods form interface:
	   // activity.afterTextChanged();
	    // A ResidenceActivity type 'is' a TextWatcher
	    // System.out.println("TextWatcher id: " + activity.id);
	    
	    // Date text changed
	    //  the static method is invoked in interface: TextWatcher.beforeTextChanged() via method in ResidenceActivity class
	    // String date = new  Date(activity.dateTextChanged).toString(); // a method in ResidenceActivity class
	    // System.out.println("Date text changed: " + date);
	    
	    // Test default interface method
	    // the method onTextChanged is invoked on an object of ResidenceActivity(which implements TextWatcher interface and its default methods)
	    // activity.onTextChanged();
	  }
	  
	}