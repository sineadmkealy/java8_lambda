package interfaces;

/**
 * A functional interface is an interface that has just one abstract method, not including 
 * methods of the parent Java Object class, default methods or static methods.
 */
import java.util.Date;

/**
 * In the Java8 programming language, an interface is a reference type, similar
 * to a class, that can contain only constants, method signatures, default
 * methods, static methods, and nested types. Method bodies exist only for
 * default methods and static methods.
 * 
 * Interfaces cannot be instantiated�they can only be implemented by classes or
 * extended by other interfaces
 * ----------------------------------------------------------------------------- 
 * This contrasts with Java 7 in which the only
 * valid entity in an interface type has been one or more abstract method
 * declarations:
 * 
 * Interface abstract methods have implied qualifiers public and abstract so
 * that void afterTextChanged() could have been written in the more verbose
 * style public abstract void afterTextChanged().
 * 
 * The Android TextWatcher interface is an example of a non-functional interface because it has three abstract methods.
 */
public interface TextWatcher_AnonClass {

	// const declaration
	// All constant values defined in an interface are implicitly public,
	// static, and final
	int id = 1;

	// static method
	// A static method is a method that is associated with the class in which it
	// is defined rather than with any object.
	// Every instance of the class shares its static methods.
	// easier for you to organize helper methods in your libraries: you can keep
	// static methods specific to an interface
	// in the same interface rather than in a separate class
	static long beforeTextChanged() {
		return new Date().getTime();
	}

	// default method
	// Default methods facilitate backward compatibility with older Java releases.
	// In �the strictest sense�, Default methods are a step backwards because they allow you to �pollute� your interfaces with code. 
	// But they provide the most elegant and practical way to allow backwards compatibility. 
	// It made it much easier for Oracle to update all the Collections classes and for you to retrofit your existing code for Lambda.
	default void onTextChanged() {
		System.out.println("I hate change");
	}

	// Java7 abstract method declaration: also a functional interface
	void afterTextChanged(); // public abstract void afterTextChanged()
}